#include "WindowUtils.h"

// Window utility functions
// Read more about these at https://stackoverflow.com/a/21767578

HWND GetMainWindow(unsigned long process_id)
{
    HandleData data;
    data.processId = process_id;
    data.hWnd = 0;
    EnumWindows(EnumWindowsCallback, (LPARAM)&data);
    return data.hWnd;
}

BOOL CALLBACK EnumWindowsCallback(HWND handle, LPARAM lParam)
{
    HandleData& data = *(HandleData*)lParam;
    unsigned long process_id = 0;
    GetWindowThreadProcessId(handle, &process_id);
    if (data.processId != process_id || !IsMainWindow(handle))
        return TRUE;
    data.hWnd = handle;
    return FALSE;
}

BOOL IsMainWindow(HWND handle)
{
    return GetWindow(handle, GW_OWNER) == (HWND)0 && IsWindowVisible(handle);
}