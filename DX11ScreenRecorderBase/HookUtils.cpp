#include "Windows.h"
#include "HookUtils.h"

#define ABSOLUTE_JMP_LENGTH 12
#define DISPLACEMENT_JMP_LENGTH 5

bool Within2GB(uintptr_t src, uintptr_t dst);
size_t CreateDisplacementJump(void* pLocation, void* pJumpDestination);
size_t CreateAbsoluteJump64(void* pLocation, void* pJumpDestination);

/*
Creates a jump instruction at a particular location that points at the destination address
Returns the number of bytes used to create the jump */
size_t CreateJump(void* pLocation, void* pJumpDestination)
{
#if _WIN64
    // If we're on 64 bit and the src and dst addresses aren't within +/-2gb of each other then use a 64 bit specific jmp
    if (!Within2GB((uintptr_t)pLocation, (uintptr_t)pJumpDestination)) {
        return CreateAbsoluteJump64(pLocation, pJumpDestination);
    }
#endif
    return CreateDisplacementJump(pLocation, pJumpDestination);
}

/*
Creates a displacement jump to a given destination address at a specific memory location
Returns the number of bytes used to create the jump */
size_t CreateDisplacementJump(void* pLocation, void* pJumpDestination) {
    DWORD oldProtect;
    VirtualProtect(pLocation, DISPLACEMENT_JMP_LENGTH, PAGE_EXECUTE_READWRITE, &oldProtect);

    *(BYTE*)pLocation = (BYTE)0xE9; // Displacement jmp
    // E9 jmps are relative to the next instruction. This is why we take off the size of the jmp instruction in this calculation
    *(int*)((uintptr_t)pLocation + 1) = (int)((uintptr_t)pJumpDestination - (uintptr_t)pLocation - DISPLACEMENT_JMP_LENGTH);

    VirtualProtect(pLocation, DISPLACEMENT_JMP_LENGTH, oldProtect, &oldProtect);
    return DISPLACEMENT_JMP_LENGTH;
}

/*
Creates a 64 bit specific jump  to an absolute address at a specific memory location
Returns the number of bytes used to create the jump */
size_t CreateAbsoluteJump64(void* pLocation, void* pJumpDestination) {
    DWORD oldProtect;
    VirtualProtect(pLocation, ABSOLUTE_JMP_LENGTH, PAGE_EXECUTE_READWRITE, &oldProtect);
    /*
    Non-displacement jmp to get around the +/- 2gb displacement limit
    mov rax, pJumpDestination ; 10 bytes
    jmp rax ; 2 bytes */
    BYTE movRax[] = { 0x48, 0xB8 }; //mov rax, 
    memcpy(pLocation, movRax, ARRAYSIZE(movRax));
    *(uintptr_t*)((uintptr_t)pLocation + ARRAYSIZE(movRax)) = (uintptr_t)pJumpDestination; // Write the destination address so that we complete our mov instruction

    BYTE jmpRax[] = { 0xFF, 0xE0 }; //jmp rax
    memcpy((void*)((uintptr_t)pLocation + ARRAYSIZE(movRax) + sizeof(uintptr_t)), jmpRax, ARRAYSIZE(jmpRax)); // Write the jmp rax instruction immediately after our mov instruction

    VirtualProtect(pLocation, ABSOLUTE_JMP_LENGTH, oldProtect, &oldProtect);
    return ABSOLUTE_JMP_LENGTH;
}

// Determines whether two addresses are within 2gb of each other
bool Within2GB(uintptr_t src, uintptr_t dst)
{
    ptrdiff_t difference = dst - src;
    return (difference <= INT_MAX && difference >= INT_MIN);
}

/*
Creates a trampoline function used to handle jumping to and from a hook function
The trampoline function also handles running the instructions replaced by our jmp in the target function
pTarget - A pointer to the area of code to hook
pHook - A pointer to the beginning of the hook function
ppHookJump - Returns the address of a pointer to the start of the trampoline's code block used to get to the hook function
ppReturnToTargetJump - Returns the address of a pointer to the trampoline's code block used to return to the target function */
void CreateTrampoline(void* pTarget, void* pHook, void** ppHookJump, void** ppReturnToTargetJump)
{
    CreateTrampoline(pTarget, pHook, ppHookJump, ppReturnToTargetJump, DISPLACEMENT_JMP_LENGTH);
}

/*
Creates a trampoline function used to handle jumping to and from a hook function
The trampoline function also handles running the instructions replaced by our jmp in the target function 
pTarget - A pointer to the area of code to hook
pHook - A pointer to the beginning of the hook function
ppHookJump - Returns the address of a pointer to the start of the trampoline's code block used to get to the hook function
ppReturnToTargetJump - Returns the address of a pointer to the trampoline's code block used to return to the target function
targetCopyLength - Number of bytes to copy from pTarget to our trampoline. This should be the number of bytes required to rebuild the instructions altered/replaced by the jump inserted at pTarget */
void CreateTrampoline(void* pTarget, void* pHook, void** ppHookJump, void** ppReturnToTargetJump, int targetCopyLength)
{
    // Search memory near the target function for a free page where we can create our trampoline
    // This approach simplifies things by guaranteeing that we can use a jmp near (E9 opcode)
    LPVOID pMemorySearch = (LPVOID)((uintptr_t)pTarget - 0xF000);
    LPVOID pInstructionPointer = nullptr;
    while (!pInstructionPointer)
    {
        *ppReturnToTargetJump = pInstructionPointer = VirtualAlloc(pMemorySearch, 1, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE); // Size is 1 since we're allocating a full page (4096 bytes) anyway
        pMemorySearch = (LPVOID)((uintptr_t)pMemorySearch + 0x1000); // Check the next page. 0x1000 is the default page size
    }

    // Add the bytes we're overwriting in our target function to the top of our trampoline
    // It's essential these instructions are still ran so that we don't run into stack alignment issues etc
    void* originalBytes = malloc(targetCopyLength);
    memcpy(originalBytes, pTarget, targetCopyLength);
    memcpy(pInstructionPointer, originalBytes, targetCopyLength);

    pInstructionPointer = (void*)((uintptr_t)pInstructionPointer + targetCopyLength);

    // Insert a jump back to the target function
    CreateJump(pInstructionPointer, (void*)((uintptr_t)pTarget + targetCopyLength));
    *ppHookJump = pInstructionPointer = (void*)((uintptr_t)pInstructionPointer + DISPLACEMENT_JMP_LENGTH);
    // Insert a jump to our hook function
    CreateJump(pInstructionPointer, pHook);
}