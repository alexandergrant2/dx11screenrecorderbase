#include "imgui.h"
#include "Overlay.h"

#include "SwapchainVMTIndices.h"
#include "HookUtils.h"
#include "WindowUtils.h"
#include "Hooks.h"

PresentFunc ReturnToPresent;                           // Function pointer that's used to return to Present in PresentHook
ResizeBuffersFunc ReturnToResizeBuffers;               // Function pointer that's used to return to ResizeBuffers in ResizeBuffersHook
SetFullscreenStateFunc ReturnToSetFullscreenState;     // Function pointer that's used to return to SetFullscreenState in SetFullscreenStateHook
GetBufferFunc ReturnToGetBuffer;                       // Function pointer that's used to return to GetBuffer in GetBufferHook

// Installs a jump at the beginning of IDXGISwapChain::Present that goes to PresentHook (below) through a dynamically created trampoline function
// Also initialises the function pointer used by the hook to return to Present
bool HookPresent(void* pPresent)
{
    void* pPresentHookJump = nullptr;
    CreateTrampoline(pPresent, PresentHook, &pPresentHookJump, (void**)&ReturnToPresent);
    return CreateJump(pPresent, pPresentHookJump);
}

// Installs a jump at the beginning of IDXGISwapChain::ResizeBuffers that goes to ResizeBuffersHook (below) through a dynamically created trampoline function
// Also initialises the function pointer used to return back to ResizeBuffers
bool HookResizeBuffers(void* pResizeBuffers)
{
    void* pResizeBuffersHookJump = nullptr;
    CreateTrampoline(pResizeBuffers, ResizeBuffersHook, &pResizeBuffersHookJump, (void**)&ReturnToResizeBuffers);
    return CreateJump(pResizeBuffers, pResizeBuffersHookJump);
}

// Installs a jump at the beginning of IDXGISwapChain::GetBuffer that goes to GetBufferHook (below) through a dynamically created trampoline function
// Also initialises the function pointer used by the hook to return to GetBuffer
bool HookGetBuffer(void* pGetBuffer)
{
    void* pGetBufferHookJump = nullptr;
    // We need to patch a different number of bytes in GetBuffer depending on the architecture used
    // It's possible these numbers might not work for some DirectX11 builds due to potential differences in the function prologues between builds (but they work on all I've tested so far)
    // Using MS Detours would be a much more reliable to do this but I wanted to do it manually as an exercise
#if _WIN64
    CreateTrampoline(pGetBuffer, GetBufferHook, &pGetBufferHookJump, (void**)&ReturnToGetBuffer);
#else
    CreateTrampoline(pGetBuffer, GetBufferHook, &pGetBufferHookJump, (void**)&ReturnToGetBuffer, 7);
#endif
    return CreateJump(pGetBuffer, pGetBufferHookJump);
}

// Installs a jump at the beginning of IDXGISwapChain::SetFullscreenState that goes to SetFullscreenStateHook (below) through a dynamically created trampoline function
// Also initialises the function pointer used by the hook to return to SetFullscreenState
bool HookSetFullscreenState(void* pSetFullscreenState)
{
    void* pSetFullscreenStateHookJump = nullptr;
    // We need to patch a different number of bytes in SetFullscreenState depending on the architecture used
    // It's possible these numbers might not work for some DirectX11 builds due to potential differences in the function prologues between builds (but they work on all I've tested so far)
    // Using MS Detours would be a much more reliable to do this but I wanted to do it manually as an exercise
#if _WIN64
    CreateTrampoline(pSetFullscreenState, SetFullscreenStateHook, &pSetFullscreenStateHookJump, (void**)&ReturnToSetFullscreenState, 9);
#else
    CreateTrampoline(pSetFullscreenState, SetFullscreenStateHook, &pSetFullscreenStateHookJump, (void**)&ReturnToSetFullscreenState);
#endif
    return CreateJump(pSetFullscreenState, pSetFullscreenStateHookJump);
}

// Hook code that's ran every time IDXGISwapChain::Present is called
// More details on IDXGISwapChain::Present and when it should be called at https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-present
HRESULT __stdcall PresentHook(IDXGISwapChain* pThis, UINT SyncInterval, UINT Flags)
{
    static bool init = false;
    if (!init)
    {
        if (!InitImGui(pThis))
            return FALSE;
        init = true;
    }

    // Run per-frame code here
    DrawOverlay();
    return ReturnToPresent(pThis, SyncInterval, Flags);
}

// Hook code that's ran every time IDXGISwapChain::ResizeBuffers is called
// More details on IDXGISwapChain::ResizeBuffers and when it should be called at https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-resizebuffers
HRESULT __stdcall ResizeBuffersHook(IDXGISwapChain* pThis, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags)
{
    // Handle screen resizing here
    return ReturnToResizeBuffers(pThis, BufferCount, Width, Height, NewFormat, SwapChainFlags);
}

// Hook code ran that's every time IDXGISwapChain::GetBuffer is called
// More details on IDXGISwapChain::GetBuffer at https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-setfullscreenstate
void __stdcall GetBufferHook(IDXGISwapChain* pThis, UINT Buffer, REFIID riid, void** ppSurface)
{
    return ReturnToGetBuffer(pThis, Buffer, riid, ppSurface);
}

// Hook code that's ran every time IDXGISwapChain::SetFullscreenState is called
// More details on IDXGISwapChain::SetFullscreenState at https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-getbuffer
HRESULT __stdcall SetFullscreenStateHook(IDXGISwapChain* pThis, BOOL Fullscreen, IDXGIOutput* pTarget)
{
    return ReturnToSetFullscreenState(pThis, Fullscreen, pTarget);
}

// Determines addresses for IDXGISwapChain::Present, IDXGISwapChain::ResizeBuffers, IDXGISwapChain::GetBuffer and IDXGISwapChain::SetFullscreenState and installs hooks
bool InstallD3DHooks()
{
    // Creates a bare minimum DXGI_SWAP_CHAIN_DESC used to create our own swapchain. We use this solely to get the addresses of swapchain functions from its virtual method table
    // Many of the values below don't really have much significance. They're just setting required fields with valid values
    // BufferUsage and BufferDesc.Format are taken from this tutorial https://docs.microsoft.com/en-us/windows/win32/direct3d11/overviews-direct3d-11-devices-initialize
    D3D_FEATURE_LEVEL featLevel;
    DXGI_SWAP_CHAIN_DESC sd{ 0 };
    sd.BufferCount = 1;
    sd.BufferUsage = DXGI_USAGE_BACK_BUFFER;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.Height = 1;
    sd.BufferDesc.Width = 1;
    sd.OutputWindow = (HWND)1;
    sd.Windowed = TRUE;
    sd.SampleDesc.Count = 1;

    ID3D11Device* pDevice = nullptr;
    IDXGISwapChain* pSwapchain = nullptr;

    // Easiest option for creating a swapchain according to https://docs.microsoft.com/en-us/windows/win32/direct3d11/overviews-direct3d-11-devices-create-swap-chain
    HRESULT hr = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_REFERENCE, nullptr, 0, nullptr, 0, D3D11_SDK_VERSION, &sd, &pSwapchain, &pDevice, &featLevel, nullptr);
    if (FAILED(hr))
        return false;

    // Get swapchain virtual method table as pointer array by dereferencing pSwapchain
    uintptr_t* pSwapVMT = *(uintptr_t**)pSwapchain;

    // Retrieve locations of Present, ResizeBuffers, GetBuffer and SetFullScreenState functions from the swapchain VMT
    void* pPresent = (void*)pSwapVMT[(int)SwapChainVMTIndices::Present];
    void* pResizeBuffers = (void*)pSwapVMT[(int)SwapChainVMTIndices::ResizeBuffers];
    void* pGetBuffer = (void*)pSwapVMT[(int)SwapChainVMTIndices::GetBuffer];
    void* pSetFullScreenState = (void*)pSwapVMT[(int)SwapChainVMTIndices::SetFullscreenState];

    SafeRelease(pSwapchain);
    SafeRelease(pDevice);

    return HookPresent(pPresent) && HookResizeBuffers(pResizeBuffers) && HookGetBuffer(pGetBuffer) && HookSetFullscreenState(pSetFullScreenState);
}