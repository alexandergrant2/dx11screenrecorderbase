#include "imgui.h"
#include "imgui_impl_dx11.h"
#include "imgui_impl_win32.h"
#include <d3d11.h>
#include "WindowUtils.h"
#include "Overlay.h"

WNDPROC oWndProc;

void DrawMainWindow();

// Initialises ImGui with the graphics objects and window handlers it needs
bool InitImGui(IDXGISwapChain* pSwapchain)
{
    ID3D11Device* pDevice = nullptr;
    ID3D11DeviceContext* pContext = nullptr;
    HRESULT hr = pSwapchain->GetDevice(__uuidof(ID3D11Device), (void**)&pDevice);
    if (FAILED(hr))
	    return false;

    pDevice->GetImmediateContext(&pContext);

    HWND window = GetMainWindow(GetCurrentProcessId());
    oWndProc = (WNDPROC)SetWindowLongPtr(window, GWLP_WNDPROC, (LONG_PTR)WndProc);
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();

    ImGui_ImplWin32_Init(window);
    ImGui_ImplDX11_Init(pDevice, pContext);

    return true;
}

// Rendering for the entire overlay. This would include all UI components/windows
void DrawOverlay()
{
    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();

    ImGui::NewFrame();

    DrawMainWindow();

    ImGui::EndFrame();
    ImGui::Render();

    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

// A basic window with a 'Start Recording' button
void DrawMainWindow()
{
    // Specify a default position/size in case there's no data in the .ini file.
    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(130, 37), ImGuiCond_FirstUseEver);

    // Main body of the main window starts here.
    ImGui::Begin("Screen Recorder", (bool*)true, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize);
    ImGui::Button("Start Recording");
    ImGui::End();
}

LRESULT __stdcall WndProc(const HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hWnd, uMsg, wParam, lParam))
        return true;
    return CallWindowProc(oWndProc, hWnd, uMsg, wParam, lParam);
}