#pragma once
#include <Windows.h>
#include <d3d11.h>

#define SafeRelease(x) if(x) { x->Release(); x = NULL; }

// Reference https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-present
HRESULT __stdcall PresentHook(IDXGISwapChain* pThis, UINT SyncInterval, UINT Flags);
// Reference https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-resizebuffers
HRESULT __stdcall ResizeBuffersHook(IDXGISwapChain* pThis, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags);
// Reference https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-setfullscreenstate
HRESULT __stdcall SetFullscreenStateHook(IDXGISwapChain* pThis, BOOL Fullscreen, IDXGIOutput* pTarget);
// Reference https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nf-dxgi-idxgiswapchain-getbuffer
void __stdcall GetBufferHook(IDXGISwapChain* pThis, UINT Buffer, REFIID riid, void** ppSurface);

// Function pointer definitions
typedef HRESULT(__stdcall* PresentFunc)(IDXGISwapChain* pThis, UINT SyncInterval, UINT Flags);
typedef HRESULT(__stdcall* ResizeBuffersFunc)(IDXGISwapChain* pThis, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags);
typedef HRESULT(__stdcall* SetFullscreenStateFunc)(IDXGISwapChain* pThis, BOOL Fullscreen, IDXGIOutput* pTarget);
typedef void(__stdcall* GetBufferFunc)(IDXGISwapChain* pThis, UINT Buffer, REFIID riid, void** ppSurface);

bool HookPresent(void* pPresent);
bool HookResizeBuffers(void* pResizeBuffers);
bool HookGetBuffer(void* pGetBuffer);
bool HookSetFullscreenState(void* pSetFullscreenState);
bool InstallD3DHooks();