#pragma once
#include "Windows.h"

struct HandleData {
    unsigned long processId;
    HWND hWnd;
};

HWND GetMainWindow(unsigned long process_id);
BOOL CALLBACK EnumWindowsCallback(HWND handle, LPARAM lParam);
BOOL IsMainWindow(HWND handle);