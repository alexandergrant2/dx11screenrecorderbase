#pragma once

enum class SwapChainVMTIndices {
    // Entries 0-7 are functions from:
    // IUnknown (0-2) (https://docs.microsoft.com/en-us/windows/win32/api/unknwn/nn-unknwn-iunknown)
    // IDXGIObject (3-6) https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nn-dxgi-idxgiobject
    // IDXGIDeviceSubObject (7) https://docs.microsoft.com/en-us/windows/win32/api/dxgi/nn-dxgi-idxgidevicesubobject
	Present = 8,
	GetBuffer = 9,
	SetFullscreenState = 10,
	GetFullscreenState = 11,
	GetDesc = 12,
	ResizeBuffers = 13,
	ResizeTarget = 14,
	GetContainingOutput = 15,
	GetFrameStatistics = 16,
	GetLastPresentCount = 17,
};