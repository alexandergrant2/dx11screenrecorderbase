#pragma once

/*
Creates a jump instruction at a particular location that points at the destination address
Returns the number of bytes used to create the jump */
size_t CreateJump(void* pLocation, void* pJumpDestination);

/*
Creates a trampoline function used to handle jumping to and from a hook function
The trampoline function also handles running the instructions replaced by our jmp in the target function
pTarget - A pointer to the area of code to hook
pHook - A pointer to the beginning of the hook function
ppHookJump - Returns the address of a pointer to the start of the trampoline's code block used to get to the hook function
ppReturnToTargetJump - Returns the address of a pointer to the trampoline's code block used to return to the target function
targetCopyLength - Number of bytes to copy from pTarget to our trampoline. This should be the number of bytes required to rebuild the instructions replaced by the jmp inserted at pTarget */
void CreateTrampoline(void* pTarget, void* pHook, void** ppHookJump, void** ppReturnToTargetJump);
void CreateTrampoline(void* pTarget, void* pHook, void** ppHookJump, void** ppReturnToTargetJump, int targetCopyLength);